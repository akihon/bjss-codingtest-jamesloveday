package com.bjss.codingtest;

public final class Offer {

    public double reduction;
    public Item extraItem;
    public int condition;
    public int totalItem;

    public Offer withReduction(double v) {
        reduction = v;
        return this;
    }


    public Offer withExtraItem(Item extraItem) {
        this.extraItem = extraItem;
        return this;
    }

    public Offer withCondition(int condition) {
        this.condition = condition;
        return this;
    }


    public Offer withTotalItem(int totalItem) {
        this.totalItem = totalItem;
        return this;
    }
}
