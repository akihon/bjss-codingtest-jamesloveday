package com.bjss.codingtest;

import java.util.HashMap;
import java.util.Map;

public final class CurrentOffers {

    public static Map<Item, Offer> currentOffersMap;

    static {
        currentOffersMap = new HashMap<>();
        currentOffersMap.put(new Item().withName("Apples").withPrice(1.00),
                new Offer().withReduction(0.1));
        currentOffersMap.put(new Item().withName("Soup").withPrice(0.65),
                new Offer().withExtraItem(new Item().withName("Bread").withPrice(0.40)).withCondition(2));
    }
}
