package com.bjss.codingtest;

import java.util.*;
import java.util.stream.Collectors;

public class BasketTotallerImpl implements BasketTotaller {

    RecieptPrinter printer;

    public double subTotal;
    public String itemName;
    public double nowTotal;

    public Double getTotal(Basket basket) {
        if (!basket.itemList.isEmpty()) {
            return basket.itemList.stream().mapToDouble(item -> item.price).sum();
        }
        return 0.00;
    }

    public double applyFinalDiscounts(Basket basket) {
        if( basket.itemList.isEmpty() ) return 0.00;

        var printer = new RecieptPrinterImpl();
        var builder = new StringBuilder();
        var total = getTotal(basket);
        //set the subtotal as the original amount
        subTotal = total;

        Set<Map.Entry<Item, Offer>> offers = CurrentOffers.currentOffersMap.entrySet();

        builder.append("Subtotal:  ").append("£").append(String.format("%.2f", total)).append("\n");

        //get the items from the basket
        boolean found = false;
       for(Item item: basket.itemList) {
           for(Map.Entry<Item, Offer> offer: offers) {
               if(offer.getKey().name.equals(item.name)) {
                   found = true;
                   break;
               }
           }
       }

       if(found) {
           basket.itemList.forEach(item -> {
               //check that an item is in the offers
               offers.forEach(action -> {
                   if (action.getKey().name.equals(item.name)) { //item found apply discounts
                       if (action.getValue().reduction > 0) {  //reduce the subtotal by the reduction
                           subTotal = (subTotal - action.getValue().reduction);
                           var paddedString = padString(action.getValue().reduction);
                           builder.append(item.name).append(" ").append(paddedString).append("%")
                                   .append(" ").append("off: ").append("-").append(paddedString).append("p").append("\n");
                       } else {
                           //item in the offers look for a condition bogof!
                           //if the item is repeated and the count matches the condition for the offer
                           //apply the offer, get the extra item add that to the basket and recalculate the subtotal

                       }
                   }
               });

           });
       } else {
           //when there are no offers for the basket!
           builder.append("(No offers available)").append("\n");
       }

        builder.append("Total: ").append("£").append(String.format("%.2f", subTotal)).append("\n");
        printer.printReciept(builder);
        return subTotal;
    }

    private int padString(double s) {
        var amount = s * 100;
        return (int) amount;
    }
}
