package com.bjss.codingtest;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class TotallerTest {

    private BasketTotallerImpl totaller;
    private Basket basket;
    private Item apples;
    private Item soup;
    private Item milk;
    private Item bread;

    @BeforeEach
    public void setup() {
        apples = new Item().withName("Apples").withPrice(1.00);
        bread = new Item().withName("Bread").withPrice(0.80);
        milk = new Item().withName("Milk").withPrice(1.30);
        soup = new Item().withName("Soup").withPrice(0.65);
        totaller = new BasketTotallerImpl();
    }

    @Test
    public void testTotalValue() {
        //add items to the basket
        var itemList = List.of(apples, bread, milk);

        basket = new Basket().withItemList(itemList);

        //get the total for the basket
        double total = totaller.getTotal(basket);

        //verify the total of the basket
        assertThat(total).isEqualTo(3.10);
    }

    @Test
    public void testDiscountApplied() {
        var itemList = List.of(apples, bread, milk);

        basket = new Basket().withItemList(itemList);

        double total = totaller.applyFinalDiscounts(basket);

        assertThat(total).isEqualTo(3.00);
    }

    @Test
    public void testNoDiscountsAvailable() {
        var itemList = List.of(milk);

        basket = new Basket().withItemList(itemList);

        double total = totaller.applyFinalDiscounts(basket);

        assertThat(total).isEqualTo(1.30);
    }
}
