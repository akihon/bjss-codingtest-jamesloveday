package com.bjss.codingtest;

import java.util.List;

public final class Basket {

    public List<Item> itemList;


    public Basket withItemList(List<Item> itemList) {
        this.itemList = itemList;
        return this;
    }
}
