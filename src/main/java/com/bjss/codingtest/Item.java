package com.bjss.codingtest;

public final class Item {
    public Double price;
    public String name;

    public Item withPrice(Double price) {
        this.price = price;
        return this;
    }

    public Item withName(String name) {
        this.name = name;
        return this;
    }
}
